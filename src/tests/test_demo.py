import os


def test_has_pythonpath():
    assert os.environ['PYTHONPATH'] == ':/app/pytest-demo'
